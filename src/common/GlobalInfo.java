/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edis
 */
public class GlobalInfo {

    static {
        instance = new GlobalInfo();
    }

    private static GlobalInfo instance;

    private String selectedDatabase;
    private List<String> databaseTypes = new ArrayList<>();
    private List<String> notAutoIncrementTypes = new ArrayList<>();
    private List<String> columnTypesThatDoesntHaveSize = new ArrayList<>();

    public GlobalInfo() {
        databaseTypes.add("INT");
        databaseTypes.add("BIGINT");
        databaseTypes.add("-----------");
        databaseTypes.add("VARCHAR");
        databaseTypes.add("DATETIME");

        notAutoIncrementTypes.add("VARCHAR");
        notAutoIncrementTypes.add("DATETIME");
    }

    public static GlobalInfo getInstance() {
        return instance;
    }

    public void setSelectedDatabase(String dbName) throws Exception {
        if (dbName == null || dbName.trim().isEmpty()) {
            throw new Exception("Select database.");
        }
        selectedDatabase = dbName;
    }

    public String getSelectedDatabase() {
        return selectedDatabase;
    }

    public List<String> getNotAutoIncrementTypes() {
        return notAutoIncrementTypes;
    }

    public List<String> getDatabaseTypes() {
        return databaseTypes;
    }

    public List<String> getColumnTypesThatDoesntHaveSize() {
        return columnTypesThatDoesntHaveSize;
    }

}
