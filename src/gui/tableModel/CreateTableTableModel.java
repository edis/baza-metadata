/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.tableModel;

import domen.Column;
import dto.ColumnDTO;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author edis
 */
public class CreateTableTableModel extends AbstractTableModel {

    private String[] columnNames = {"Name", "Type", "Size", "Primary", "Not null", "Unique", "Auto increment"};
    private List<Column> columns;

    public CreateTableTableModel() {
        columns = new ArrayList<>();
    }

    @Override
    public int getRowCount() {
        return columns.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Column col = columns.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return col.getName();
            case 1:
                return col.getType();
            case 2:
                return col.getSize();
            case 3:
                return col.isPrimaryKey();
            case 4:
                return col.isNotNull();
            case 5:
                return col.isUnique();
            case 6:
                return col.isAutoIncrement();
            default:
                return "N/N";
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public void addColumn(Column column) throws Exception {
        if (columns.contains(column)) {
            throw new Exception("Column " + column.getName() + " already exists.");
        }

        if (columns.add(column)) {
            stateChanged();
        }
    }

    public void stateChanged() {
        fireTableDataChanged();
    }

    public boolean containsColumn(Column col) {
        return columns.contains(col);
    }

    public List<Column> getColumns() {
        return columns;
    }

    public boolean removeColumn(int selectedRow) throws Exception {
        if (selectedRow < 0 || selectedRow >= columns.size()) {
            throw new Exception("Invalid row selected");
        }
        if (columns.remove(selectedRow) != null) {
            stateChanged();
            return true;
        }
        return false;
    }

    public ColumnDTO getColumnAt(int i) throws Exception {
        if (i < 0 || i >= columns.size()) {
            throw new Exception("Invalid column selected");
        }

        Column c = columns.get(i);

        return new ColumnDTO(c.getName(), c.getType(), c.getSize(), c.isPrimaryKey(), c.isNotNull(), c.isUnique(), c.isAutoIncrement());
    }

    public ColumnDTO columnExists(String columnName) {
        for (Column col : columns) {
            if (col.getName().equals(columnName)) {
                ColumnDTO columnDTO = new ColumnDTO(col.getName(), col.getType(), col.getSize(), col.isPrimaryKey(), col.isNotNull(), col.isUnique(), col.isAutoIncrement());
                return columnDTO;
            }
        }
        return null;
    }

    public void updateColumn(ColumnDTO column) throws Exception {
        for (Column c : columns) {
            if (c.getName().equals(column.name)) {
                c.setName(column.name);
                c.setType(column.type);
                c.setSize(column.size);
                c.setPrimaryKey(column.primaryKey);
                c.setNotNull(column.notNull);
                c.setUnique(column.unique);
                c.setAutoIncrement(column.autoIncrement);
                stateChanged();
                return;
            }
        }
        throw new Exception("Column " + column.name + " does not exists");
    }
}
