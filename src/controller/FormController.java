package controller;

import gui.CreateDatabaseForm;
import gui.CreateTableForm;
import gui.KeyboardShortcutsForm;
import gui.QueryResultForm;
import javax.swing.JFrame;
import common.Refreshable;
import dto.ColumnDTO;
import enumeration.EnumCreateOrEditColumnState;
import gui.DbConfigForm;
import gui.CreateOrEditColumnForm;

/**
 *
 * @author edis
 */
public class FormController {

    public synchronized void openShortcutsForm(JFrame owner) {
        KeyboardShortcutsForm form = new KeyboardShortcutsForm(owner, true);
        form.setVisible(true);
    }

    public synchronized void openCreateTableForm(String dbName) {
        CreateTableForm createTableForm = new CreateTableForm();
        createTableForm.setSelectedDatabase(dbName);
        createTableForm.setVisible(true);
    }

    public synchronized void openCreateDatabaseForm(Refreshable observer) throws Exception {
        CreateDatabaseForm createDatabaseForm = new CreateDatabaseForm();
        createDatabaseForm.setObserver(observer);
        createDatabaseForm.setVisible(true);
    }

    public synchronized void openQueryResultForm(Object[][] data, Object[] columnNames) {
        QueryResultForm queryResultForm = new QueryResultForm();
        queryResultForm.setData(data, columnNames);
        queryResultForm.setVisible(true);
    }

    public synchronized void openDatabaseConfigForm() throws Exception {
        final DbConfigForm dbConfigForm = new DbConfigForm();
        dbConfigForm.setVisible(true);
    }

    public void openEditColumnForm(JFrame parent, ColumnDTO column) {
        CreateOrEditColumnForm ecForm = new CreateOrEditColumnForm(parent, true);
        ecForm.setValues(column);
        ecForm.setState(EnumCreateOrEditColumnState.UPDATE);
        ecForm.setVisible(true);
    }

    public void openCreateColumnForm(JFrame parent) {
        CreateOrEditColumnForm ecForm = new CreateOrEditColumnForm(parent, true);
        ecForm.setVisible(true);
    }

}
