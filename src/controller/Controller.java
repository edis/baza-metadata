/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.DbBroker;
import dto.DatabaseDTO;
import dto.TableDTO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import queryBuilder.CodeQueryBuilder;
import queryBuilder.IQueryBuilder;

/**
 *
 * @author edis
 */
public class Controller {

    private DbBroker db = DbBroker.getInstance();
    private IQueryBuilder queryReader;

    public Controller() throws SQLException {
        queryReader = new CodeQueryBuilder();
        if (!db.isConnected()) {
            db.connect();
        }
    }

    public List<DatabaseDTO> getAllDatabases() throws Exception {
        try {
            return db.getDatabases();
        } catch (Exception e) {
            throw e;
        } 
    }

    public boolean createDatabase(String dbName) throws Exception {
        if (dbName.trim().isEmpty()) {
            throw new Exception("Database name is empty!");
        }

        try {
            return db.executeDDL(queryReader.getCreateDatabaseQuery(dbName));
        } catch (Exception e) {
            throw e;
        }

    }

    public boolean removeDatabase(String dbName) throws Exception {
        if (dbName.trim().isEmpty()) {
            throw new Exception("Database name is empty!");
        }
        
        try {
            return db.executeDDL(queryReader.getRemoveDatabaseQuery(dbName));
        } catch (Exception e) {
            throw e;
        }
        
    }

    public Object[][] getTableData(TableDTO table) throws Exception {
        if (table == null) {
            throw new Exception("Select table");
        }

        try {
            db.setDatabase(table.databaseName);
            return db.executeSelect(table);
        } catch (SQLException sqlex) {
            throw sqlex;
        }

    }

    public boolean removeTable(String tableName) throws Exception {
        if (tableName == null || tableName.trim().isEmpty()) {
            throw new Exception("Invalid table name");
        }
        try {
            db.executeDDL(queryReader.getRemoveTableQuery(tableName.trim()));
            return true;
        } catch (SQLException sqlex) {
            throw sqlex;
        } catch (IOException ioex) {
            throw ioex;
        }
    }

}
