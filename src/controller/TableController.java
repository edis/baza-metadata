/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.DbBroker;
import domen.Column;
import domen.Database;
import domen.Table;
import java.sql.SQLException;
import java.util.List;
import queryBuilder.CodeQueryBuilder;
import validator.ColumnValidator;
import validator.DatabaseValidator;
import validator.IValidator;
import validator.TableValidator;
import queryBuilder.IQueryBuilder;

/**
 *
 * @author edis
 */
public class TableController {

    private DbBroker db = DbBroker.getInstance();
    private IQueryBuilder queryReader = new CodeQueryBuilder();

    public void addColumn(List<Column> columns, String columnName, String columnType, Integer columnSize, boolean primary, boolean notNull, boolean unique, boolean autoIncrement) throws Exception {
        try {
            Column col = new Column(columnName, columnType, columnSize);
            col.setPrimaryKey(primary);
            col.setNotNull(notNull);
            col.setUnique(unique);
            col.setAutoIncrement(autoIncrement);

            IValidator columnValidator = new ColumnValidator(col);
            columnValidator.validate();

            if (this.columnExists(columns, col)) {
                throw new Exception("Column with given name already exists");
            }

            columns.add(col);
        } catch (Exception ex) {
            throw ex;
        }
    }

    private boolean columnExists(List<Column> columns, Column col) {
        for (Column column : columns) {
            if (column.getName().equals(col.getName())) {
                return true;
            }
        }

        return false;
    }

    public boolean createTable(String databaseName, String tableName, List<Column> columns) throws Exception {
        try {
            IValidator validator;

            Database db = new Database(databaseName);

            validator = new DatabaseValidator(db);
            validator.validate();

            Table table = new Table(tableName);
            table.setColumns(columns);
            table.setDatabase(db);

            validator = new TableValidator(table);
            validator.validate();

            try {
                this.db.setDatabase(databaseName);
                return this.db.executeDDL(queryReader.getCreateTableQuery(table));
            } catch (SQLException sqle) {
                throw sqle;
            }
        } catch (Exception e) {
            throw e;
        }
    }

}
