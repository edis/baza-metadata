/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import db.DbBroker;
import db.DbConfig;
import gui.DbConfigForm;
import gui.MainForm;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.UIManager;
import util.Constants;

/**
 *
 * @author edis
 */
public class Main {

    public static void main(String[] args) throws Exception {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }

        File dbConfig = new File(Constants.DB_CONFIG_URL);
        if (!dbConfig.exists()) {
            DbConfig.getInstance().createDbConfigFile(dbConfig);
        }

        try {
            DbBroker.getInstance().connect();
            JFrame f = new MainForm();
            f.setVisible(true);
        } catch (Exception e) {
            DbConfigForm dbForm = new DbConfigForm();
            dbForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            dbForm.setVisible(true);
            e.printStackTrace();
            System.err.println("Neuspesna konekcija sa bazom!");
        }

    }
}
