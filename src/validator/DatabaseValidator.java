/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validator;

import domen.Database;

/**
 *
 * @author edis
 */
public class DatabaseValidator implements IValidator {

    private Database db;

    public DatabaseValidator(Database db) {
        this.db = db;
    }

    @Override
    public void validate() throws Exception {
        if (db == null) {
            throw new Exception("Database is null");
        }

        if (db.getName() == null || db.getName().trim().isEmpty()) {
            throw new Exception("Database name is not valid. (null or empty)");
        }
    }

}
