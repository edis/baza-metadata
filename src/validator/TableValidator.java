/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validator;

import common.GlobalInfo;
import domen.Column;
import domen.Table;
import java.util.List;

/**
 *
 * @author edis
 */
public class TableValidator implements IValidator {

    private Table table;

    public TableValidator(Table table) {
        this.table = table;
    }

    @Override
    public void validate() throws Exception {
        if (table == null) {
            throw new Exception("Not valid table.");
        }

        if (table.getName().trim().isEmpty()) {
            throw new Exception("Not valid table name.");
        }

        if (table.getName().trim().contains(".")) {
            throw new Exception("Invalid table name (remove .)");
        }

        if (tableHasNumber()) {
            throw new Exception("Invalid table name. Table name has a number.");
        }

        if (table.getColumns().isEmpty()) {
            throw new Exception("Table must have at least one column.");
        }

        if (!tableHasPrimaryKey()) {
            throw new Exception("Table must have at least one column that is primary key.");
        }

        try {
            validateColumns();
        } catch (Exception e) {
            throw e;
        }

    }

    private boolean tableHasPrimaryKey() {
        List<Column> columns = table.getColumns();
        for (Column col : columns) {
            if (col.isPrimaryKey()) {
                return true;
            }
        }

        return false;
    }

    private boolean columnTypesAreValid() throws Exception {
        List<Column> columns = table.getColumns();
        for (Column column : columns) {
            String columnType = column.getType();
            boolean contains = GlobalInfo.getInstance().getColumnTypesThatDoesntHaveSize().contains(columnType);
            if (contains) {
                throw new Exception("Column " + column.getName() + " (" + columnType + ") must not have size.");
            }
        }
        return true;
    }

    private void validateColumns() throws Exception {
        List<Column> columns = table.getColumns();
        for (Column column : columns) {
            IValidator val = new ColumnValidator(column);
            try {
                val.validate();
            } catch (Exception ex) {
                throw ex;
            }
        }
    }

    private boolean tableHasNumber() {
        char[] nameArray = table.getName().toCharArray();

        for (char c : nameArray) {
            if (Character.isDigit(c)) {
                return true;
            }
        }

        return false;
    }

}
