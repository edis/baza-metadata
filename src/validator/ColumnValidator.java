/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validator;

import common.GlobalInfo;
import domen.Column;

/**
 *
 * @author edis
 */
public class ColumnValidator implements IValidator {

    private Column column;

    public ColumnValidator(Column column) {
        this.column = column;
    }

    @Override
    public void validate() throws Exception {
        if (column == null) {
            throw new Exception("Column is null.");
        }

        if (column.getName().trim().isEmpty()) {
            throw new Exception("Column name is required.");
        }

        if (GlobalInfo.getInstance().getNotAutoIncrementTypes().contains(column.getType()) && column.isAutoIncrement()) {
            throw new Exception("Column type " + column.getType() + " must not be auto increment.");
        }

        if (column.getType().contains("-")) {
            throw new Exception("Invalid column type");
        }

        if (column.isPrimaryKey() && !column.isNotNull()) {
            throw new Exception("Primary key column must be not null.");
        }

    }

}
