/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author edis
 */
public class ColumnDTO {

    public String name = "";
    public String type = "";
    public Integer size = -1;
    public boolean primaryKey = false;
    public boolean notNull = false;
    public boolean autoIncrement = false;
    public boolean unique = false;

    public ColumnDTO() {
    }

    public ColumnDTO(String name, String type, Integer size, boolean primaryKey, boolean notNull, boolean unique, boolean autoIncrement) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.primaryKey = primaryKey;
        this.notNull = notNull;
        this.unique = unique;
        this.autoIncrement = autoIncrement;
    }

    @Override
    public String toString() {
        return name + " (" + type + "(" + size + "))";
    }

}
