/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edis
 */
public class TableDTO {

    public String name = "";
    public String databaseName = "";
    public List<ColumnDTO> columns = new ArrayList<>(20);
    public Integer rowCount = -1;
    public Object[][] data = new Object[1][1];

    public TableDTO(String name, String databaseName) {
        this.name = name;
        this.databaseName = databaseName;
    }

    @Override
    public String toString() {
        return name;
    }

    public void addColumn(ColumnDTO col) {
        columns.add(col);
    }

}
