/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edis
 */
public class DatabaseDTO {

    public String name = "";
    public List<TableDTO> tables = new ArrayList<>(100);

    public DatabaseDTO(String n) {
        name = n;
    }

    @Override
    public String toString() {
        return name;
    }

}
