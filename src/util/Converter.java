/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.List;

/**
 *
 * @author edis
 */
public abstract class Converter {

    public static String[] toStringArray(List<String> list) {
        int size = list.size();
        String[] str = new String[size];

        for (int i = 0; i < size; i++) {
            str[i] = list.get(i);
        }

        return str;
    }

}
