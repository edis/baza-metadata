/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queryBuilder;

import domen.Table;
import java.io.IOException;

/**
 *
 * @author edis
 */
public interface IQueryBuilder {

    public abstract String getCreateDatabaseQuery(String dbName) throws IOException;

    public abstract String getRemoveDatabaseQuery(String dbName) throws IOException;

    public abstract String getCreateTableQuery(Table table) throws IOException;

    public abstract String getRemoveTableQuery(String tableName) throws IOException;

}
