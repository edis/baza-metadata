/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queryBuilder;

import domen.Column;
import domen.Table;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author edis
 */
public class CodeQueryBuilder implements IQueryBuilder {

    public CodeQueryBuilder() {
    }

    @Override
    public String getCreateDatabaseQuery(String dbName) throws IOException {
        return "CREATE SCHEMA `" + dbName + "`";
    }

    @Override
    public String getRemoveDatabaseQuery(String dbName) throws IOException {
        return "DROP DATABASE `" + dbName + "`";
    }

    @Override
    public String getCreateTableQuery(Table table) throws IOException {
        StringBuffer query = new StringBuffer(80);
        query.append("CREATE TABLE `" + table.getName() + "`").append(" (");

        List<Column> columns = table.getColumns();

        for (Column column : columns) {
            query.append(column.getName()).append(" ").append(column.getType()).append(" ");

//            if (!GlobalInfo.getInstance().getColumnTypesThatDoesntHaveSize().contains(column.getType())) {
//                query.append("(").append(column.getSize()).append(") ");
            query.append("(").append(column.getSize()).append(") ");
//            }

            if (column.isAutoIncrement()) {
                query.append("AUTO_INCREMENT ");
            }

            if (column.isUnique()) {
                query.append("UNIQUE ");
            }

            if (column.isNotNull()) {
                query.append("NOT NULL");
            }

            query.append(",");
        }

        query.append("CONSTRAINT `" + table.getName() + "_pk` PRIMARY KEY (" + table.getPrimaryKeysAsString() + ")");
        query.append(");");

        return query.toString();
    }

    @Override
    public String getRemoveTableQuery(String tableName) throws IOException {
        return "DROP TABLE `" + tableName + "`;";
    }

}
