/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import dto.ColumnDTO;
import dto.DatabaseDTO;
import dto.TableDTO;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edis
 */
public class DbBroker {

    private Connection conn;
    private static DbBroker instance;

    private DbBroker() {
    }

    public boolean isConnected() throws SQLException {
        if (conn == null || conn.isClosed()) {
            return false;
        }
        return true;
    }

    public void connect() throws SQLException {
        closeConnection();
        DbConfig cfg = DbConfig.getInstance();
        conn = DriverManager.getConnection(cfg.getUrl(), cfg.getUser(), cfg.getPassword());
        conn.setAutoCommit(false);
//        System.out.println("CONNECTED !!!");
    }

    public void setDatabase(String db) throws SQLException {
        if (db == null) {
            throw new SQLException("Database name is null");
        }
        
        if (conn.getCatalog() == null) {
            throw new SQLException("Database is not selected");
        }

        if (!conn.getCatalog().equals(db)) {
            conn.setCatalog(db);
            System.out.println("CONNECTED TO DB: " + db + " !!!");
        }
    }

    public void closeConnection() throws SQLException {
        if (conn != null) {
            conn.close();
            conn = null;
        }
    }

    public static DbBroker getInstance() {
        if (instance == null) {
            instance = new DbBroker();
        }
        return instance;
    }

    public List<DatabaseDTO> getDatabases() throws Exception {
        List<DatabaseDTO> dbs = new ArrayList<>(50);

        DatabaseMetaData dbMetadata;
        try {
            dbMetadata = conn.getMetaData();
            ResultSet rs = dbMetadata.getCatalogs();

            while (rs.next()) {
                String databaseName = rs.getString(1);
                if (databaseName.equalsIgnoreCase("mysql")
                        || databaseName.equalsIgnoreCase("performance_schema")
                        || databaseName.equalsIgnoreCase("information_schema")
                        || databaseName.equalsIgnoreCase("phpmyadmin")) {
                    continue;
                }

                DatabaseDTO database = new DatabaseDTO(databaseName);

                setDatabase(databaseName);

                database.tables = getTables(databaseName);

                dbs.add(database);
            }
            rs.close();

            return dbs;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public List<TableDTO> getTables(String db) throws Exception {
        List<TableDTO> tables = new ArrayList<>(100);

        DatabaseMetaData dbMetadata;
        try {
            dbMetadata = conn.getMetaData();
            ResultSet rs = dbMetadata.getTables(db, null, null, new String[]{"TABLE"});

            setDatabase(db);

            while (rs.next()) {
                String databaseName = rs.getString(1);
                String tbl = rs.getString(3);
                TableDTO tableDTO = new TableDTO(tbl, databaseName);
                tableDTO.columns = getColumns(tableDTO.name);
                tableDTO.rowCount = getTableRowCount(tbl);
                tables.add(tableDTO);
            }
            rs.close();

            return tables;
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public List<ColumnDTO> getColumns(String tbl) throws Exception {
        List<ColumnDTO> columns = new ArrayList<>(20);

        DatabaseMetaData dbMetadata;
        try {
            dbMetadata = conn.getMetaData();
            ResultSet rs = dbMetadata.getColumns(null, null, tbl, null);

            while (rs.next()) {
                String name = rs.getString(4);
                String type = rs.getString(6);
                Integer size = rs.getInt(7);
                boolean notNull = rs.getString(18).equals("NO") ? true : false;
                boolean autoIncrement = rs.getString(23).equals("YES") ? true : false;

                //TODO: pk i unique ekseri
                ColumnDTO col = new ColumnDTO(name, type, size, false, notNull, false, autoIncrement);

                columns.add(col);
            }
            rs.close();

            return columns;
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public Integer getTableRowCount(String table) throws SQLException {
        int rowCount = -1;
        try {
            ResultSet rs = conn.createStatement().executeQuery("SELECT COUNT(*) as count FROM " + table + ";");

            if (rs.next()) {
                rowCount = rs.getInt("count");
            }
            rs.close();

        } catch (SQLException sqlEx) {
            rowCount = -1;
        }
        return rowCount;
    }

    public boolean executeDDL(String ddl) throws SQLException {
        if (conn.createStatement().executeUpdate(ddl) >= 0) {
            conn.commit();
            return true;
        }
        return false;
    }

    public Object[][] executeSelect(TableDTO table) throws SQLException {
        setDatabase(table.databaseName);

        String query = "SELECT * FROM " + table.name;

        ResultSet rs = conn.createStatement().executeQuery(query);

        rs.last();
        int numberOfRows = rs.getRow();
        rs.beforeFirst();

        int numberOfColumns = table.columns.size();

        Object[][] data = new Object[numberOfRows][numberOfColumns];

        int i = 0;
        while (rs.next()) {
            for (int j = 0; j < numberOfColumns; j++) {
                data[i][j] = rs.getObject(j + 1);
            }
            i++;
        }
        rs.close();

        return data;
    }

}
