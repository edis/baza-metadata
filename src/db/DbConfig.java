/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Properties;
import util.Constants;

/**
 *
 * @author edis
 */
public class DbConfig {

    static {
        instance = new DbConfig();
    }

    private Properties props;
    private static DbConfig instance;

    private DbConfig() {
        props = new Properties();
        try {
            load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static DbConfig getInstance() {
        if (instance == null) {
            instance = new DbConfig();
        }
        return instance;
    }

    public void load() throws IOException {
        props.load(new FileInputStream(Constants.DB_CONFIG_URL));
    }

    public void save() throws IOException {
        props.store(new BufferedWriter(new FileWriter(Constants.DB_CONFIG_URL)), "Modified: " + LocalDateTime.now().toString());
    }

    public void createDbConfigFile(File dbConfig) throws IOException {
        Properties p = new Properties();
        p.setProperty("dbms", "mysql");
        p.setProperty("host", "localhost");
        p.setProperty("port", 3306 + "");
        p.setProperty("database", "test");
        p.setProperty("user", "root");
        p.setProperty("password", "");

        p.store(new BufferedWriter(new FileWriter(dbConfig)), "Modified: " + LocalDateTime.now().toString());
    }

    public String getDbms() {
        return props.getProperty(Constants.DBMS);
    }

    public String getHost() {
        return props.getProperty(Constants.HOST);
    }

    public int getPort() throws NumberFormatException {
        return Integer.parseInt(props.getProperty(Constants.PORT + ""));
    }

    public String getDatabase() {
        return props.getProperty(Constants.DATABASE);
    }

    public String getUser() {
        return props.getProperty(Constants.USER);
    }

    public String getPassword() {
        return props.getProperty(Constants.PASSWORD);
    }

    public void setDbms(String dbms) {
        props.setProperty(Constants.DBMS, dbms);
    }

    public void setHost(String host) {
        props.setProperty(Constants.HOST, host);
    }

    public void setPort(int port) {
        props.setProperty(Constants.PORT, port + "");
    }

    public void setDatabase(String db) {
        props.setProperty(Constants.DATABASE, db);
    }

    public void setUser(String user) {
        props.setProperty(Constants.USER, user);
    }

    public void setPassword(String pass) {
        props.setProperty(Constants.PASSWORD, pass);
    }

    public String getUrl() {
        String url = "jdbc:" + getDbms() + "://" + getHost() + ":" + getPort() + "/" + getDatabase();
        return url;
    }

}
