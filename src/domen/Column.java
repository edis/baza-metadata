/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domen;

import java.util.Objects;

/**
 *
 * @author edis
 */
public class Column {

    private String name;
    private String type;
    private Integer size;
    private boolean primaryKey;
    private boolean notNull;
    private boolean unique;
    private boolean autoIncrement;

    public Column(String columnName, String columnType, Integer columnSize) throws Exception {
        setName(columnName);
        setType(columnType);
        setSize(columnSize);
        primaryKey = false;
        notNull = false;
        unique = false;
        autoIncrement = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws Exception {
        if (name == null || name.trim().isEmpty()) {
            throw new Exception("You must enter column name.");
        }
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) throws Exception {
        if (size < 0 || size >= Integer.MAX_VALUE) {
            throw new Exception("Size must be non-negative");
        }
        this.size = size;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public boolean isNotNull() {
        return notNull;
    }

    public void setNotNull(boolean notNull) throws Exception {
        this.notNull = notNull;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(boolean autoIncrement) throws Exception {
        this.autoIncrement = autoIncrement;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Column other = (Column) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    public String getSQLCommand() {
        StringBuffer sb = new StringBuffer(15);

        sb.append(name + " " + type);
        if (size > 0) {
            sb.append("(" + size + ")");
        }

        if (notNull) {
            sb.append(" NOT NULL");
        }

        if (autoIncrement) {
            sb.append(" AUTO_INCREMENT");
        }

        if (unique) {
            sb.append(" UNIQUE");
        }

        return sb.toString();
    }

}
