/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domen;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edis
 */
public class Table {

    private String name;
    private Database database;
    private List<Column> columns;

    public Table() {
        columns = new ArrayList<>();
    }

    public Table(String tableName) throws Exception {
        setName(tableName);
    }

    public void addColumn(Column col) {
        columns.add(col);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws Exception {
        if (name == null || name.trim().isEmpty()) {
            throw new Exception("You must enter table name.");
        }
        this.name = name;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    public String getPrimaryKeysAsString() {
        StringBuffer sb = new StringBuffer(15);

        List<Column> primaryKeys = getPrimaryKeys();
        int primaryKeysSize = primaryKeys.size();

        for (int i = 0; i < primaryKeysSize; i++) {
            sb.append(primaryKeys.get(i).getName());
            if (i < primaryKeysSize - 1) {
                sb.append(", ");
            }
        }

        return sb.toString();
    }

    public List<Column> getPrimaryKeys() {
        List<Column> primaryKeys = new ArrayList<>();

        for (Column column : columns) {
            if (column.isPrimaryKey()) {
                primaryKeys.add(column);
            }
        }

        return primaryKeys;
    }
}
